#include <Adafruit_NeoPixel.h>

#define DATAPIN 7
#define INTERUPTPIN 2
#define COLORPIN A2
#define SPEEDPIN A1

Adafruit_NeoPixel strip = Adafruit_NeoPixel(47, DATAPIN, NEO_GRB + NEO_KHZ800);

int state = 0;
bool stateChanged = false; 
int maxState = 5;

int potPower = 0;    
int potColor = 0;    

int centerWidth = 1;        
int i = 0;           

//color picker
uint32_t Wheel(byte WheelPos) {
  if (WheelPos < 85) {
    return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
    WheelPos -= 170;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

void getPotColor() {
  potColor = analogRead(COLORPIN);
  potColor = map(potColor, 0, 1023, 1, 255);
  Serial.print("potColor: ");
  Serial.println(potColor);
}

void getPotPower() {
  potPower = analogRead(SPEEDPIN);
  potPower = map(potPower, 0, 1023, 5, 255);
  Serial.print("potPower: ");
  Serial.println(potPower);
}

void setup() {
  pinMode(INTERUPTPIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERUPTPIN), changeMode, RISING);

  pinMode(COLORPIN, INPUT);
  pinMode(SPEEDPIN, INPUT);

  Serial.begin(9600);

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void changeMode() {
  Serial.println("INTERUPT");
  if (stateChanged) {
    return;
  }
  state++;
  stateChanged = true;
  if (state > maxState) {
    state = 0;
  }
}

void loop() {

  getPotColor();
  getPotPower();

  strip.setBrightness(potPower);

  if (i > strip.numPixels()) {
    i = 0;
  }

  switch (state) {
    case 0:
      runBack();
      break;
    case 1:
      centerToOut();
      break;
    case 2:
      nblink();
      break;
    case 3:
      rainbow();
      break;
    case 4:
      police();
      break;
    case 5:
      chaos();
      break;
  }

  strip.show();

  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }

  i++;
  stateChanged = false;
  delay(20);
}

void runBack() {
  strip.setPixelColor(i,   Wheel(potColor));
  strip.setPixelColor(i + 1, Wheel(potColor));
  strip.setPixelColor(i + 2, Wheel(potColor));
  strip.setPixelColor(i + 3, Wheel(potColor));
  strip.setPixelColor(i + 4, Wheel(potColor));
  strip.setPixelColor(i + 5, Wheel(potColor));
  strip.setPixelColor(i + 6, Wheel(potColor));
}


void centerToOut() {
  centerWidth += random(-4, 5);

  if (centerWidth < 0) {
    centerWidth = 0;
  }

  if (centerWidth > 22) {
    centerWidth = 22;
  }

  for (int j = 0; j < centerWidth; j++) {
    strip.setPixelColor(22 - j, Wheel(((j * 9 + potColor))));
    strip.setPixelColor(23 + j, Wheel(((j * 9 + potColor))));
  }

  delay(20);
}

void nblink() {
  for (int j = 0; j < strip.numPixels(); j += 4) {
    int pos = i + j;

    if (pos > strip.numPixels()) {
      pos = pos - strip.numPixels();
    }
    strip.setPixelColor(pos,  Wheel(potColor));
  }
}


void rainbow() {
  for (int q = 0; q < strip.numPixels(); q++) {
    strip.setPixelColor(q, Wheel(((i * 256 / strip.numPixels()) + q * 6) & 255));
  }
}

void police() {
  if (i % 2 == 0) {
    for (int q = 0; q < strip.numPixels(); q++) {
      strip.setPixelColor(q, strip.Color(255, 0, 0));
    }
  } else {
    for (int q = 0; q < strip.numPixels(); q++) {
      strip.setPixelColor(q, strip.Color(0, 0, 255));
    }
  }

  delay(150);
}

void chaos() {
  for (int q = 0; q < strip.numPixels(); q++) {
    if (random(100) > 80) {
      strip.setPixelColor(q, Wheel(random(255)));
    }
  }
}
